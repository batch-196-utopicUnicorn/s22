console.log("Hello World");

//Array Methods

//JS has built-in methods to manipulate and manage our arrays.

//Mutator Methods
//Mutator methods updates/mutates our array.

//.push() - push() method will allows us to add an item at the end of our array.

let koponanNiEugene = ["Eugene"];

koponanNiEugene.push("Vincent");
console.log(koponanNiEugene);


//The use of push method also returns a value, the update length of the array:
console.log(koponanNiEugene.push("Dennis"));
console.log(koponanNiEugene);


//.pop() - pop() method will allow us to remove an item at the end of the array and return the item that was declared.

let removeItem = koponanNiEugene.pop();

console.log(koponanNiEugene);
console.log(removeItem);


//.unshift() - unshift() method allows us to add an item at the front of our array.

let fruits = ["Mango", "Kiwi", "Apple", "Grapes"];
fruits.unshift("Pineapple");
console.log(fruits)

//.shift() - shift() methods allows us to remove an item at the beginning of our array

let pcBrands = ["Apple", "Acer", "Asus", "Dell"];
pcBrands.shift();
console.log(pcBrands);

//.splice() - simultaneously remove elements from a specified index and add elements
//.splice(startingIndex) - all items from the starting index will be deleted.
pcBrands.splice(1);
console.log(pcBrands);

//.splice(startingIndex, deleteCount) - deletes a specified number of item from a starting index.
fruits.splice(0,1);
console.log(fruits);

//.splice(startingIndex, 0, elements to be added) - add items from a starting index without deleting.
console.log(koponanNiEugene)
koponanNiEugene.splice(1, 0, "Dennis", "Alfred");
console.log(koponanNiEugene);

//.splice(startingIndex, deleteCount, elementsToBeAdded) - Delete an indicated number of items from a starting index and add elements.
console.log(fruits)
fruits.splice(0, 2, "Lime", "Cherry")
console.log(fruits);

//splice, when removing any element, will be returned the removed elements.
let item = fruits.splice();
console.log(fruits);
console.log(item);


let spiritDetective = koponanNiEugene.splice(0, 1);
console.log(spiritDetective);
console.log(koponanNiEugene);

//sort() - sort our elements in an alphanumeric order

let members = ["Ben", "Alan", "Alvin", "Jino", "Tine"];
members.sort();
console.log(members);

//Note: .sort() method by itself is used mostly for sorting arrays of strings. It performs differently when sorting numbers. There is a separate algorithm for doing so.

let numbers = [50, 100, 12, 10, 1];
numbers.sort();
console.log(numbers);


//.reverse() - reverses the order of array elements
members.reverse();
console.log(members);

//Non-Mutator Methods
//These methods are not able to modify or change the original array when they are used.

let carBrands = ["Vios", "Fortuner", "Crosswind", "City", "Vios", "Starex"];

//indexOf()
//Return the index number of the first matching element in the array.
//If no match was found, it will return -1.
let firstIndexOfVios = carBrands.indexOf("Vios");
console.log(firstIndexOfVios);

//very useful in finding the index of item when the total length of the array is unknown and if the array is constantly being added into or manipulated.
let indexOfStarex = carBrands.indexOf("Starex");
console.log(indexOfStarex);

let indexOfBeetle = carBrands.indexOf("Beetle");
console.log(indexOfBeetle)

//lastIndexOf()
//Returns the index number of the last matching element in the array

let lastIndexOfVios = carBrands.lastIndexOf("Vios");
console.log(lastIndexOfVios);//4

let indexOfMio = carBrands.lastIndexOf("Mio");
console.log(indexOfMio);//-1

//.slice()
//copy a slice/portion of an array and return a new array from it.

let shoeBrand = ["Jordan", "Nike", "Adidas", "Converse", "Sketchers"];

//slice(startingIndex) = allows us to copy an array into a new array with items from the startingIndex to the last item.
let myOwnedShoes = shoeBrand.slice(1);
console.log(myOwnedShoes);
//Non-mutator methods do not update the original array
console.log(shoeBrand);
//slice(startingIndex,endingIndex) = allows us to copy an array into a new array with items from the starting index to just before the endingIndex
let herOwnedShoes = shoeBrand.slice(2,4);
console.log(herOwnedShoes);

let heroes = ["Captain America", "Superman", "Spiderman", "Wonder Woman", "Hulk", "Hawkeye", "Dr. Strange"];

let myFavoriteHeroes = heroes.slice(2, 6)
console.log(myFavoriteHeroes)

//toString()
//return our array as a string separated by commas
let superHeroes = heroes.toString();
console.log(superHeroes);
console.log("My favourite heroes are " + superHeroes);

//join()
//Returns our array as a string by specified separator

//without a specified separator:
//it defaults to separate elements with comma
let superHeroes2 = heroes.join();
console.log(superHeroes2);

let superHeroes3 = heroes.join(" ");
console.log(superHeroes3);

let superHeroes4 = heroes.join(1 );
console.log(superHeroes4);


//Iterator Methods
//These methods iterates or loops over the items in an array.

//.forEach()
//Similar to a for loop wherein it is able to iterate over the items in an array.
//It is able to repeat an action FOR EACH item in the array.

//forEach() takes an argument which is a function. This function has no name and cannot be invoked outside our forEach(), this is whaht we call an anonymous function.
//forEach() will run the function FOR EACH item in the array.
//the function insude forEach is ablt to receive the current item being looped.

let counter = 0;
heroes.forEach(function(hero){
	counter ++;
	console.log(counter)
	console.log(hero);
})

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

chessBoard.forEach(function(row){
	row.forEach(function(square){
		console.log(square)
	})
})


let numArr = [5,12,30,46,40];

numArr.forEach(function(number){

	if(number % 5 ===0){
		console.log(number + " is divisible by 5");
	}else{
		console.log(number + " is not divisible by 5");
	}
})


//map()
//Similar to forEach() it will iterate over all items in an array and run a function for each item. However, with a map, whatever is returned in the function will be added into a new array we can save.

// function without a name is an anonymous function

// let members = ["Ben", "Alan", "Alvin", "Jino", "Tine"];
let instructors = members.map(function(member){

	return member + " is an instructor";

})

//map() returns a new array which contains the value/data returned by the function that was for each item in the original way

console.log(instructors);
console.log(members);

let numArr2 = [1, 2 ,3 , 4 ,5];

let squareMap = numArr2.map(function(number){

	return number * number;

});

console.log(squareMap);


//map() vs forEach()
//map() is able to return a new array
//forEach() simply iterates and does not return anything

let squareForEach = numArr2.forEach(function(number){

	return number * number

})

console.log(squareForEach);

//includes()
//includes() returns a boolean which determines if the item is in the array or not.
//returns true if it is.
//else, returns false

let isAMember = members.includes("Tine");
console.log(isAMember);

let isAMember2 = members.includes("Tee Jae");
console.log(isAMember2);
